<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if ($request->has('type'))
        {
            return Customer::with('taxEntry', 'addresses')->where('type', 'like', $request->type)->get();
        }
        else {
            return Customer::with('taxEntry', 'addresses')->get();
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $input = $request->all();
        Customer::create($input);

        return response()->json([
            'res' => true,
            'message' => 'Customer successfully created.'
        ], 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $yaloId
     * @return \Illuminate\Http\Response
     */
    public function show($yaloId)
    {
        $customer = Customer::with('taxEntry', 'addresses')->where('yaloId', '=', $yaloId)->first();
        return $customer;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $yaloId
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $yaloId)
    {
        $input = $request->all();
        $customer = Customer::where('yaloId', '=', $yaloId)->first();
        $customer->update($input);

        return response()->json([
            'res' => true,
            'message' => 'Customer successfully updated.'
        ], 201); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
