<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $primaryKey = 'addressId';

    protected $table = 'Addresses';

    public $timestamps = false;

    protected $fillable = [
        'position',
        'name',
        'fullAddress',
        'latitude',
        'longitude'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customerId', 'customerId');
    }
}
