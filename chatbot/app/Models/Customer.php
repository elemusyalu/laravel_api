<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;

    protected $primaryKey = 'customerId';

    protected $table = 'Customers';

    public $timestamps = false;

    protected $fillable = [
        'isActive',
        'yaloId',
        'code',
        'name',
        'phoneNumber',
        'type'
    ];

    public function getTaxInfoAttribute()
    {
        return taxEntry();
    }

    /**
     * Get the tax entry associated with the customer.
     */
    public function taxEntry()
    {
        return $this->hasOne('App\Models\TaxEntry', 'customerId', 'customerId');
    }

    /**
     * Get the addresses for the customer.
     */
    public function addresses()
    {
        return $this->hasMany('App\Models\Address', 'customerId', 'customerId');
    }
}
