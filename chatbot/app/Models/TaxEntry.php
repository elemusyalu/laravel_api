<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TaxEntry extends Model
{
    use HasFactory;

    protected $primaryKey = 'entryId';

    protected $table = 'TaxEntries';

    public $timestamps = false;

    protected $fillable = [
        'taxId',
        'sendMail',
        'email'
    ];

    public function customer()
    {
        return $this->belongsTo('App\Models\Customer', 'customerId', 'customerId');
    }
}
